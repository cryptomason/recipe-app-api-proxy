# recipe-app-api-proxy

Recipe app API proxy application

## Usage

### Environmental Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP-HOST` - Hostname of the app to forward request to (default: `app`)
* `APP-PORT` - Port of the app to forward request to (default; `9000`)



